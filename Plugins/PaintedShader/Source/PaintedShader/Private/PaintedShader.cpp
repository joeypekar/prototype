// Copyright Epic Games, Inc. All Rights Reserved.

#include "PaintedShader.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"
#include "Logging/LogMacros.h"
#include "Misc/Paths.h"

#define LOCTEXT_NAMESPACE "FPaintedShaderModule"

void FPaintedShaderModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	//#if (ENGINE_MINOR_VERSION >= 21)
	FString ShaderDirectory = FPaths::Combine(FPaths::ProjectDir(), TEXT("Plugins/PaintedShader/Shaders"));
	AddShaderSourceDirectoryMapping("/Plugins", ShaderDirectory);
	//#endif
}

void FPaintedShaderModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FPaintedShaderModule, PaintedShader)